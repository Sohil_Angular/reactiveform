import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class AuthserviceService {

  constructor(private router: Router) { }

  isAuthenticed(): boolean {
    let localstoreAbc = localStorage.getItem("googleIn");
    if (JSON.parse(localstoreAbc) == 1) {
      return true;
    } else {
      return false;
    }
  }
  login() {
    let getDatafromLocalStore = localStorage.getItem("googleIn");
    if (JSON.parse(getDatafromLocalStore) == 1) {
      this.router.navigate(['/reactiveform'])
    } else {
        let setData = 1;
        localStorage.setItem("googleIn", JSON.stringify(setData));
        this.router.navigate(['/reactiveform'])
    }
  }
}
