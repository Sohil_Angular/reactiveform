import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardGuard } from './auth/auth-guard.guard';
import { LoginComponent } from './login/login.component';
import { ReactiveFormComponentComponent } from './reactive-form-component/reactive-form-component.component';

const routes: Routes = [

  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'reactiveform',
    component: ReactiveFormComponentComponent,
    canActivate: [AuthGuardGuard],
  },
  {
    path: '',
    component: LoginComponent,
    canActivate: [AuthGuardGuard],
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)], // configure the router
  exports: [RouterModule]
})
export class AppRoutingModule { }
