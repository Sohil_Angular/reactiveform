import { Component, NgZone, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthserviceService } from '../auth/authservice.service'
import { environment } from '../../environments/environment'
import { Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username;
  userEmail;
  counter = 0;
  constructor(ngZone: NgZone, private authService: AuthserviceService, private metaservice: Meta) {
    this.signInNgzone(ngZone);
  }

  ngOnInit(): void {
    this.setMetaTags();
  }

  setMetaTags() {
    this.metaservice.addTags([
      { name: environment.name, content: environment.client_secretKey }
    ])

    console.log(environment.client_secretKey);
  }
  signInNgzone(ngZone) {
    window['onSignIn'] = user => ngZone.run(() => {
      this.onSignIn(user);
    })
  }
  onSignIn(googleUser) {
    this.counter++;
    var profile = googleUser.getBasicProfile();
    this.username = profile.getName();
    this.userEmail = profile.getEmail();

    let getvalue = localStorage.getItem("googleIn");

    if (JSON.parse(getvalue) == 1) {
      this.authService.login();
    } 

    if (this.counter == 2) {
      this.authService.login();
    }
  }

}
