import { Component, OnInit } from '@angular/core';
import { FormControl, FormArray, FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms'
@Component({
  selector: 'app-dynamic-form-control',
  templateUrl: './dynamic-form-control.component.html',
  styleUrls: ['./dynamic-form-control.component.css']
})
export class DynamicFormControlComponent implements OnInit {

  employeeDetail: FormArray;
  employeeForm: FormGroup;
  selectedValue: string;
  primaryValues = []
  
  constructor(private formB: FormBuilder) { }

  ngOnInit(): void {
    this.primarymethod();
  }

  primarymethod(){
    this.primaryValues=[
      {name:'email'},
      {name:'id'},
      {name:'name'}
     ]
     this.employeeForm = this.formB.group({
      employeeDetail: this.formB.array([this.generateForm()])
    });
  }

  generateForm() {
    return this.formB.group({
      Email: ['', Validators.compose([Validators.required,Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')])],
      FirstName: ['', Validators.compose([Validators.required, Validators.pattern('[A-Za-z]+')])],
      LastName: ['', Validators.compose([Validators.required, Validators.pattern('[A-Za-z]+')])],
      GurdianName: ['', Validators.compose([Validators.required, Validators.pattern('[A-Za-z]+')])],
      Grade: ['', Validators.compose([Validators.required, Validators.pattern("[0-9]*")])],
      ContactNumber: ['', Validators.compose([Validators.required, Validators.pattern("[0-9]*"),Validators.maxLength(10)])],
      DialCode: ['',Validators.compose([Validators.required, Validators.pattern("[0-9]*",),Validators.maxLength(5)])],
      PrimaryValue: ['']
    });
  }
  addForm() {

    this.employeeDetail = this.employeeForm.get('employeeDetail') as FormArray;
    this.employeeDetail.push(this.generateForm());
  }
  get getControls() {
    return this.employeeForm.controls.employeeDetail;
  }

  get employeeControls() {
    return this.employeeForm.get('employeeDetail')['controls'];
  }

  removeForm(i) {
    this.employeeDetail.removeAt(i);
  }

  onSubmit(dataValue) {

    console.log(dataValue);
  }
  

}
