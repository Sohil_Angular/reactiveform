import { Component,OnInit } from '@angular/core';
import { from } from 'rxjs';
import { MatCalendarCellClassFunction, MatDatepickerInputEvent } from '@angular/material/datepicker';
import { AbstractControl, FormControl, FormGroup, Validators, ValidatorFn, FormArray, NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-reactive-form-component',
  templateUrl: './reactive-form-component.component.html',
  styleUrls: ['./reactive-form-component.component.css']
})
export class ReactiveFormComponentComponent implements OnInit {

  firstnameString: string;
  lastnameString: string;
  emailString: string;
  mobilenumber: string;
  userDetail: FormGroup;
  languageString: string;
  selectedValue: string;
  multicheck: string;
  constructor(private routerActive:Router) { }
  
  ngOnInit(): void {

    this.userDetail = new FormGroup({
      firstname: new FormControl(''),
      lastname: new FormControl(''),
      email: new FormControl(''),
      mobile: new FormControl(''),
      age: new FormControl('', [ageValidator]),  
      language: new FormControl( 
        [
          'English',
           'Hindi', 
           'Gujarati'
          ])
    });
   
  }
  onSubmit(formvalue: NgForm) {
    this.languageString = this.userDetail.value.language
    this.multicheck = this.selectedValue;
    
    this.firstnameString = formvalue.value.firstname;
    this.lastnameString = formvalue.value.lastname;
    this.emailString = formvalue.value.email;
    this.mobilenumber = formvalue.value.mobile;
  }
  clearstorage(){
    this.routerActive.navigate(['/login'])
    localStorage.clear();
  }
}


function ageValidator(selectdate: AbstractControl): { [key: string]: boolean } | null {
  var updateDate = new Date();
  console.log(selectdate.value);   
  if (selectdate.value != "") {
    let inputDateYear = selectdate.value.getFullYear();
    let currentDateYear = updateDate.getFullYear();
    let calculateage = (currentDateYear - inputDateYear);
    if (calculateage >= 18) {
      return { 'ageValidator': true };
    }
  }
  return null;
}
